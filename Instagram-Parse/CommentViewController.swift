//
//  CommentViewController.swift
//  Instagram-Parse
//
//  Created by Ibraheem Hindi on 8/24/18.
//  Copyright © 2018 Ibraheem Hindi. All rights reserved.
//

import UIKit
import Parse

class CommentViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var table_view: UITableView!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    @IBOutlet weak var no_comments_yet: UILabel!
    @IBOutlet weak var comment_field: UITextField!
    @IBOutlet weak var post_button: UIButton!
    
    var in_network_call = false
    var post_uuid = ""
    var comments = [Comment]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        table_view.dataSource = self
        table_view.delegate = self
        table_view.tableFooterView = UIView()
        
        // Tap anywhere to hide the keyboard
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        self.view.addGestureRecognizer(recognizer)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        safelyFetchComments()
    }
    
    @objc func hideKeyboard() {
        self.view.endEditing(true)
    }
    
    func circleImage(image_view: UIImageView){
        image_view.layer.cornerRadius = image_view.frame.size.width / 2
        image_view.clipsToBounds = true
    }
    
    func noConnection(operation: String){
        let alert = UIAlertController(
            title: "Network Error",
            message: "Please check your internet connection",
            preferredStyle: .alert
        )
        
        alert.addAction(UIAlertAction(title: "Retry", style: .cancel) {UIAlertAction in
            if operation == "FETCH"{
                self.safelyFetchComments()
            }
            else if operation == "POST"{
                self.safelyPostComment()
            }
            
        })
        self.present(alert, animated: true, completion: nil)
    }
    
    func safelyFetchComments(){
        let app_delegate = UIApplication.shared.delegate as! AppDelegate
        if !app_delegate.isConnected(){
            noConnection(operation: "FETCH")
        }
        else{
            if !in_network_call{
                in_network_call = true
                fetchComments()
            }
        }
    }
    
    func safelyPostComment(){
        let app_delegate = UIApplication.shared.delegate as! AppDelegate
        if !app_delegate.isConnected(){
            noConnection(operation: "POST")
        }
        else{
            if !in_network_call{
                in_network_call = true
                uploadComment()
            }
        }
    }
    
    // Specify number of rows
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comments.count
    }
    
    // Specify custom cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "comment_cell", for: indexPath) as! CommentCell
        
        let target_comment = comments[indexPath.row]
        let author_parts = target_comment.by.split(separator: "@")
        circleImage(image_view: cell.author_pic)
        
        cell.comment = target_comment
        cell.comment_label.text = target_comment.text
        cell.author_label.text = String(author_parts[0])
        cell.time_label.text = target_comment.timestamp
        cell.author_pic.image = UIImage(data: target_comment.author_pic)
        return cell
    }
    
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onCommentChanged(_ sender: Any) {
        post_button.isEnabled = (comment_field.text != "")
    }
    
    @IBAction func onCommentEntered(_ sender: Any) {
        resignFirstResponder()
    }
    
    @IBAction func postComment(_ sender: Any) {
        safelyPostComment()
    }
    
    func showSimpleAlert(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel) {UIAlertAction in})
        self.present(alert, animated: true, completion: nil)
    }
    
    func currentTimeStamp() -> String{
        let now = Date()
        let formatter = DateFormatter()
        
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = "yyyy-MM-dd HH:mm"
        return formatter.string(from: now)
    }
    
    func uploadComment(){
        loader.startAnimating()
        no_comments_yet.alpha = 0
        table_view.alpha = 0
        
        let new_comment = Comment()
        new_comment.by = PFUser.current()!.username!
        new_comment.text = self.comment_field.text!
        new_comment.timestamp = self.currentTimeStamp()
        new_comment.author_pic = PFUser.current()!["pic"] as! Data
        
        let comment_json = PFObject(className: "Comments")
        comment_json["post_uuid"] = post_uuid
        comment_json["by"] = new_comment.by
        comment_json["text"] = new_comment.text
        comment_json["timestamp"] = new_comment.timestamp
        comment_json["author_pic"] = new_comment.author_pic
        
        comment_json.saveInBackground(block: { (success, error) in
            self.table_view.alpha = 1
            if error != nil{
                self.showSimpleAlert(title: "Error", message: (error?.localizedDescription)!)
                
                if self.comments.count == 0{
                    self.no_comments_yet.alpha = 1
                }
            }
            else if success{
                self.loader.stopAnimating()
                self.comments.append(new_comment)
                self.table_view.reloadData()
                self.comment_field.text = ""
                self.post_button.isEnabled = false
            }
            
            self.in_network_call = false
        })
    }
    
    func fetchComments(){
        table_view.alpha = 0
        no_comments_yet.alpha = 0
        comments.removeAll()
        loader.startAnimating()
        
        let query = PFQuery(className: "Comments")
        query.whereKey("post_uuid", equalTo: post_uuid)
        query.findObjectsInBackground { (results, error) in
            self.loader.stopAnimating()
            
            if error != nil{
                self.showSimpleAlert(title: "Error", message: (error?.localizedDescription)!)
            }
            else{
                if results!.count == 0{
                    self.no_comments_yet.alpha = 1
                }
                else{
                    for item in results!{
                        let current_comment = Comment()
                        current_comment.by = item["by"] as! String
                        current_comment.text = item["text"] as! String
                        current_comment.timestamp = item["timestamp"] as! String
                        current_comment.author_pic = item["author_pic"] as! Data
                        
                        self.comments.append(current_comment)
                    }
                    
                    self.table_view.alpha = 1
                    self.table_view.reloadData()
                }
            
                self.in_network_call = false
            }
        }
        
    }

}

