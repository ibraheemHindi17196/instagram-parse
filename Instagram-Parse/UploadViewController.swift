//
//  SecondViewController.swift
//  Instagram-Parse
//
//  Created by Ibraheem Hindi on 8/23/18.
//  Copyright © 2018 Ibraheem Hindi. All rights reserved.
//

import UIKit
import Parse

class UploadViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var image_view: UIImageView!
    @IBOutlet weak var caption_field: UITextField!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    @IBOutlet weak var post_button: UIButton!
    
    let app_delegate = UIApplication.shared.delegate as! AppDelegate
    var has_image = false
    var image = UIImage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Adjust placeholder
        caption_field.attributedPlaceholder = NSAttributedString(
            string: "Caption",
            attributes: [
                .foregroundColor : UIColor.lightGray,
                .font : UIFont(name: "Avenir Next", size: 25)!
            ]
        )
        
        // Attach listener to image view
        image_view.isUserInteractionEnabled = true
        let tap_recognizer = UITapGestureRecognizer(target: self, action: #selector(selectImage))
        image_view.addGestureRecognizer(tap_recognizer)
        
        // Tap anywhere to hide the keyboard
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        self.view.addGestureRecognizer(recognizer)
    }
    
    @objc func hideKeyboard() {
        self.view.endEditing(true)
    }
    
    func noConnection(failed_action: String){
        let alert = UIAlertController(
            title: "Network Error",
            message: "Please check your internet connection",
            preferredStyle: .alert
        )
        
        alert.addAction(UIAlertAction(title: "Retry", style: .cancel) {UIAlertAction in
            if failed_action == "UPLOAD"{
                self.uploadPost()
            }
            else if failed_action == "LOGOUT"{
                self.logoutUser()
            }
        })
        self.present(alert, animated: true, completion: nil)
    }
    
    func showSimpleAlert(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel) {UIAlertAction in
            if title == "Success"{
                let feed_vc = self.tabBarController!.viewControllers![0] as! ProfileViewController
                feed_vc.updated = false
                self.tabBarController?.selectedIndex = 0
                
                // Cleanup
                self.image = UIImage(named: "tap_to_select")!
                self.image_view.image = self.image
                self.caption_field.text = ""
            }
        })
        self.present(alert, animated: true, completion: nil)
    }
    
    func uploadPost(){
        if !app_delegate.isConnected(){
            noConnection(failed_action: "UPLOAD")
        }
        else{
            if caption_field.text == ""{
                showSimpleAlert(title: "Error", message: "Please provide a caption")
            }
            else if !has_image{
                showSimpleAlert(title: "Error", message: "Please select an image")
            }
            else{
                post_button.isEnabled = false
                loader.startAnimating()
                
                let user_pic = PFUser.current()!["pic"] as! Data
                let image_file = UIImageJPEGRepresentation(image, 0.5)!
                let email = PFUser.current()!.username!
                let uuid = UUID().uuidString
                
                let post = PFObject(className: "Posts")
                post["uuid"] = email + uuid
                post["author"] = email
                post["caption"] = caption_field.text!
                post["image"] = image_file
                post["likes"] = [String]()
                post["author_pic"] = user_pic
                
                post.saveInBackground{ (success, error) in
                    self.loader.stopAnimating()
                    self.post_button.isEnabled = true
                    
                    if error != nil{
                        self.showSimpleAlert(title: "Error", message: (error?.localizedDescription)!)
                    }
                    else if success{
                        self.showSimpleAlert(title: "Success", message: "Post uploaded successfully")
                    }
                }
            }
        }
    }
    
    @objc func selectImage(){
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = .photoLibrary
        picker.allowsEditing = true
        present(picker, animated: true, completion: nil)
    }
    
    // After selecting an image
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        image = (info[UIImagePickerControllerEditedImage] as? UIImage)!
        image_view.image = image
        has_image = true
        self.dismiss(animated: true, completion: nil)
    }
    
    func logoutUser(){
        if !app_delegate.isConnected(){
            noConnection(failed_action: "LOGOUT")
        }
        else{
            self.loader.startAnimating()
            
            PFUser.logOutInBackground { (error) in
                self.loader.stopAnimating()
                if error != nil{
                    self.showSimpleAlert(title: "Error", message: (error?.localizedDescription)!)
                }
                else{
                    UserDefaults.standard.removeObject(forKey: "email")
                    UserDefaults.standard.synchronize()
                    
                    let board = UIStoryboard(name: "Main", bundle: nil)
                    let login_vc = board.instantiateViewController(withIdentifier: "login_vc") as! LoginViewController
                    self.app_delegate.window?.rootViewController = login_vc
                }
            }
        }
    }
    
    @IBAction func onCaptionEntered(_ sender: Any) {
        resignFirstResponder()
    }
    
    @IBAction func post(_ sender: Any) {
        uploadPost()
    }
    
    @IBAction func logout(_ sender: Any) {
        logoutUser()
    }
    
}

