//
//  FirstViewController.swift
//  Instagram-Parse
//
//  Created by Ibraheem Hindi on 8/23/18.
//  Copyright © 2018 Ibraheem Hindi. All rights reserved.
//

import UIKit
import Parse

class ProfileViewController:
    UIViewController,
    UITableViewDelegate,
    UITableViewDataSource,
    UIImagePickerControllerDelegate,
    UINavigationControllerDelegate{
    
    @IBOutlet weak var profile_image: UIImageView!
    @IBOutlet weak var username_label: UILabel!
    
    @IBOutlet weak var table_view: UITableView!
    @IBOutlet weak var no_posts_yet: UILabel!
    @IBOutlet weak var loader: UIActivityIndicatorView!

    var posts = [Post]()
    var posts_json = [PFObject]()
    var delete_index = IndexPath()
    var comments_index = IndexPath()
    var selected_image = UIImage()
    let app_delegate = UIApplication.shared.delegate as! AppDelegate
    
    var updated = false
    var in_call = false
    
    
    override func viewDidAppear(_ animated: Bool) {
        safelyFetchPosts()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setup username
        let email = PFUser.current()!.username!
        let email_parts = email.split(separator: "@")
        username_label.text = String(email_parts[0])

        // Setup table view
        table_view.dataSource = self
        table_view.delegate = self
        table_view.tableFooterView = UIView()
        table_view.separatorColor = .black
        table_view.register(UINib(nibName: "FeedCell", bundle: nil), forCellReuseIdentifier: "FeedCell")
        
        // Setup user profile pic
        circleImage(image_view: profile_image)
        profile_image.isUserInteractionEnabled = true
        let tap_recognizer = UITapGestureRecognizer(target: self, action: #selector(selectProfileImage))
        profile_image.addGestureRecognizer(tap_recognizer)
        let pic = PFUser.current()!["pic"]
        profile_image.image = UIImage(data: pic as! Data)
    }
    
    // Table View -> Specify number of rows
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    // Table View -> Specify custom cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FeedCell", for: indexPath) as! FeedCell
        let post = posts[indexPath.row]
        
        cell.profile_container = self
        cell.feed_container = nil
        
        cell.post = post
        cell.index = indexPath
        
        let author_parts = post.author.split(separator: "@")
        cell.author_label.text = String(author_parts[0])
        self.circleImage(image_view: cell.author_image)
        cell.author_image.image = UIImage(data: cell.post.author_pic)
        
        cell.caption_label.text = post.caption
        cell.post_image.image = UIImage(data: post.image)
        cell.likes_count.text = "\(post.likes.count) Likes"
        
        // Update heart icon
        let username = PFUser.current()!.username!
        let liked = cell.post.likes.contains(username)
        if liked{
            cell.like_button.setImage(UIImage(named: "like"), for: .normal)
        }
        else{
            cell.like_button.setImage(UIImage(named: "unlike"), for: .normal)
        }
        
        return cell
    }
    
    // Table View -> Delete row
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            delete_index = indexPath
            confirmPostDeletion()
        }
    }
    
    // Profile Pic
    func circleImage(image_view: UIImageView){
        image_view.layer.cornerRadius = image_view.frame.size.width / 2
        image_view.clipsToBounds = true
    }
    
    // Profile Pic
    @objc func selectProfileImage(){
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = .photoLibrary
        picker.allowsEditing = true
        present(picker, animated: true, completion: nil)
    }
    
    // Profile Pic
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        selected_image = info[UIImagePickerControllerEditedImage] as! UIImage
        self.dismiss(animated: true, completion: nil)
        safelyUploadImage()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toComments"{
            let cell = table_view.cellForRow(at: comments_index) as! FeedCell
            let destination = segue.destination as! CommentViewController
            destination.post_uuid = cell.post.uuid
            destination.modalTransitionStyle = .crossDissolve
        }
    }
    
    func goToComments(index: IndexPath){
        comments_index = index
        performSegue(withIdentifier: "toComments", sender: self)
    }
    
    func confirmPostDeletion(){
        let alert = UIAlertController(
            title: "Confirm Deletion",
            message: "Are you sure you want to delete this post?",
            preferredStyle: .alert
        )
        
        alert.addAction(UIAlertAction(title: "Delete", style: .default) {UIAlertAction in
            self.safelyDeletePost()
        })
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) {UIAlertAction in})
        self.present(alert, animated: true, completion: nil)
    }
    
    func showSimpleAlert(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel) {UIAlertAction in})
        self.present(alert, animated: true, completion: nil)
    }

    func safelyUploadImage(){
        if !in_call{
            if !app_delegate.isConnected(){
                let network_alert = UIAlertController(
                    title: "Network Error",
                    message: "Please check your internet connection",
                    preferredStyle: .alert
                )
                
                network_alert.addAction(UIAlertAction(title: "Retry", style: .cancel) {UIAlertAction in
                    self.safelyUploadImage()
                })
                
                self.present(network_alert, animated: true, completion: nil)
            }
            else{
                self.loader.startAnimating()
                let current_user = PFUser.current()!
                let pic_data = UIImageJPEGRepresentation(selected_image, 0.5)!
                current_user["pic"] = pic_data
                current_user.saveInBackground { (success, error) in
                    if error != nil{
                        self.showSimpleAlert(title: "Error", message: (error?.localizedDescription)!)
                    }
                    else{
                        self.profile_image.image = self.selected_image
                        
                        // Update all posts
                        for post in self.posts_json{
                            post["author_pic"] = pic_data
                            post.saveInBackground()
                        }
                        
                        // Update all comments
                        let comments_query = PFQuery(className: "Comments")
                        comments_query.whereKey("by", equalTo: current_user.username!)
                        comments_query.findObjectsInBackground(block: { (comments, error) in
                            if error == nil{
                                for comment in comments!{
                                    comment["author_pic"] = pic_data
                                    comment.saveInBackground()
                                }
                            }
                        })
                        
                        for post in self.posts_json{
                            post["author_pic"] = pic_data
                            post.saveInBackground()
                        }
                        
                        for cell in self.table_view.visibleCells as! [FeedCell]{
                            cell.author_image.image = self.selected_image
                        }
                        
                        self.loader.stopAnimating()
                        self.in_call = false
                    }
                }
            }
        }
    }
    
    func safelyFetchPosts(){
        if !updated && !in_call{
            if !app_delegate.isConnected(){
                
                let network_alert = UIAlertController(
                    title: "Network Error",
                    message: "Please check your internet connection",
                    preferredStyle: .alert
                )
                
                network_alert.addAction(UIAlertAction(title: "Retry", style: .cancel) {UIAlertAction in
                    self.safelyFetchPosts()
                })
                
                self.present(network_alert, animated: true, completion: nil)
            }
            else{
                
                // Now it is safe to fetch posts
                
                in_call = true
                table_view.alpha = 0
                no_posts_yet.alpha = 0
                posts.removeAll()
                loader.startAnimating()
                
                let query = PFQuery(className: "Posts")
                query.whereKey("author", equalTo: PFUser.current()!.username!)
                query.findObjectsInBackground { (posts_list, error) in
                    if error != nil{
                        self.showSimpleAlert(title: "Error", message: (error?.localizedDescription)!)
                    }
                    else{
                        if posts_list?.count == 0{
                            self.no_posts_yet.alpha = 1
                        }
                        else{
                            self.table_view.alpha = 1
                            self.posts_json = posts_list!
                        }
                        
                        for post in posts_list!{
                            let post_obj = Post()
                            post_obj.uuid = post["uuid"] as! String
                            post_obj.author = post["author"] as! String
                            post_obj.caption = post["caption"] as! String
                            post_obj.image = post["image"] as! Data
                            post_obj.likes = post["likes"] as! [String]
                            post_obj.author_pic = post["author_pic"] as! Data
                            
                            self.posts.append(post_obj)
                        }
                        
                        self.loader.stopAnimating()
                        self.table_view.reloadData()
                        self.updated = true
                        self.in_call = false
                    }
                }
            }
        }
    }
    
    func safelyToggleLike(index: IndexPath){
        if !in_call{
            if !app_delegate.isConnected(){
                
                let network_alert = UIAlertController(
                    title: "Network Error",
                    message: "Please check your internet connection",
                    preferredStyle: .alert
                )
                
                network_alert.addAction(UIAlertAction(title: "Retry", style: .cancel) {UIAlertAction in
                    self.safelyToggleLike(index: index)
                })
                
                self.present(network_alert, animated: true, completion: nil)
            }
            else{
                
                // Now it is safe to toggle the like
                
                let cell = table_view.cellForRow(at: index) as! FeedCell
                let username = PFUser.current()!.username!
                let liked = cell.post.likes.contains(username)
                loader.startAnimating()
                
                let query = PFQuery(className: "Posts")
                query.whereKey("uuid", equalTo: cell.post.uuid)
                query.findObjectsInBackground { (results, error) in
                    if error != nil{
                        self.showSimpleAlert(title: "Error", message: (error?.localizedDescription)!)
                    }
                    else{
                        let target = results!.first!
                        var likes = target["likes"] as! [String]
                        
                        if !liked{
                            likes.append(username)
                        }
                        else{
                            if let index = likes.index(of: username) {
                                likes.remove(at: index)
                            }
                        }
                        
                        target["likes"] = likes
                        target.saveInBackground(block: { (success, error) in
                            if error != nil{
                                self.showSimpleAlert(title: "Error", message: (error?.localizedDescription)!)
                            }
                            else if success{
                                if let index = cell.post.likes.index(of: username) {
                                    cell.post.likes.remove(at: index)
                                }
                                
                                if liked{
                                    cell.like_button.setImage(UIImage(named: "unlike"), for: .normal)
                                }
                                else{
                                    cell.like_button.setImage(UIImage(named: "like"), for: .normal)
                                    cell.post.likes.append(username)
                                }
                                
                                cell.likes_count.text = "\(cell.post.likes.count) Likes"
                            }
                            
                            self.loader.stopAnimating()
                            self.in_call = false
                        })
                    }
                }
            }
        }
    }
    
    func safelyDeletePost(){
        if !in_call{
            if !app_delegate.isConnected(){
                let network_alert = UIAlertController(
                    title: "Network Error",
                    message: "Please check your internet connection",
                    preferredStyle: .alert
                )
                
                network_alert.addAction(UIAlertAction(title: "Retry", style: .cancel) {UIAlertAction in
                    self.safelyDeletePost()
                })
                
                self.present(network_alert, animated: true, completion: nil)
            }
            else{
                
                // Now it is safe to delete the post
                
                table_view.alpha = 0
                no_posts_yet.alpha = 0
                loader.startAnimating()
                
                let cell_to_delete = table_view.cellForRow(at: delete_index) as! FeedCell
                let delete_query = PFQuery(className: "Posts")
                delete_query.whereKey("uuid", equalTo: cell_to_delete.post.uuid)
                delete_query.findObjectsInBackground { (results, error) in
                    if error != nil{
                        self.showSimpleAlert(title: "Error", message: (error?.localizedDescription)!)
                    }
                    else{
                        results?.first?.deleteInBackground(block: { (success, error) in
                            if error != nil{
                                self.showSimpleAlert(title: "Error", message: (error?.localizedDescription)!)
                            }
                            else{
                                // Delete from tableview
                                self.posts.remove(at: self.delete_index.row)
                                self.table_view.reloadData()
                                
                                if self.posts.count == 0{
                                    self.no_posts_yet.alpha = 1
                                }
                                else{
                                    self.table_view.alpha = 1
                                }
                            }
                            
                            self.loader.stopAnimating()
                            self.in_call = false
                        })
                    }
                }
            }
        }
    }
    
    func safelyLogout(){
        if !in_call{
            if !app_delegate.isConnected(){
                let network_alert = UIAlertController(
                    title: "Network Error",
                    message: "Please check your internet connection",
                    preferredStyle: .alert
                )
                
                network_alert.addAction(UIAlertAction(title: "Retry", style: .cancel) {UIAlertAction in
                    self.safelyLogout()
                })
                
                self.present(network_alert, animated: true, completion: nil)
            }
            else{
                
                // Now it is safe to logout
                
                loader.startAnimating()
                no_posts_yet.alpha = 0
                
                PFUser.logOutInBackground { (error) in
                    if error != nil{
                        self.showSimpleAlert(title: "Error", message: (error?.localizedDescription)!)
                    }
                    else{
                        UserDefaults.standard.removeObject(forKey: "email")
                        UserDefaults.standard.synchronize()
                        
                        let app_delegate = UIApplication.shared.delegate as! AppDelegate
                        let board = UIStoryboard(name: "Main", bundle: nil)
                        let login_vc = board.instantiateViewController(withIdentifier: "login_vc") as! LoginViewController
                        app_delegate.window?.rootViewController = login_vc
                    }
                    
                    self.loader.stopAnimating()
                    self.in_call = false
                }
            }
        }
    }
    
    @IBAction func logout(_ sender: Any) {
        safelyLogout()
    }
    
}

