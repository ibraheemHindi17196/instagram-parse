//
//  FeedViewController.swift
//  Instagram-Parse
//
//  Created by Ibraheem Hindi on 8/26/18.
//  Copyright © 2018 Ibraheem Hindi. All rights reserved.
//

import UIKit
import Parse

class FeedViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var table_view: UITableView!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    @IBOutlet weak var no_posts_yet: UILabel!
    
    var in_call = false
    var posts = [Post]()
    var comments_index = IndexPath()
    let app_delegate = UIApplication.shared.delegate as! AppDelegate
    
    
    override func viewDidAppear(_ animated: Bool) {
        safelyFetchPosts()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        table_view.dataSource = self
        table_view.delegate = self
        table_view.tableFooterView = UIView()
        table_view.separatorColor = .black
        table_view.register(UINib(nibName: "FeedCell", bundle: nil), forCellReuseIdentifier: "FeedCell")
    }
    
    func showSimpleAlert(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel) {UIAlertAction in})
        self.present(alert, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toFeedComments"{
            let cell = table_view.cellForRow(at: comments_index) as! FeedCell
            let destination = segue.destination as! CommentViewController
            destination.post_uuid = cell.post.uuid
            destination.modalTransitionStyle = .crossDissolve
        }
    }
    
    func goToComments(index: IndexPath){
        comments_index = index
        performSegue(withIdentifier: "toFeedComments", sender: self)
    }
    
    func circleImage(image_view: UIImageView){
        image_view.layer.cornerRadius = image_view.frame.size.width / 2
        image_view.clipsToBounds = true
    }
    
    @IBAction func logout(_ sender: Any) {
        safelyLogout()
    }
    
    // Table View -> Specify number of rows
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    // Table View -> Specify custom cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FeedCell", for: indexPath) as! FeedCell
        let post = posts[indexPath.row]
        
        cell.feed_container = self
        cell.profile_container = nil
        
        cell.post = post
        cell.index = indexPath
        
        let author_parts = post.author.split(separator: "@")
        cell.author_label.text = String(author_parts[0])
        self.circleImage(image_view: cell.author_image)
        cell.author_image.image = UIImage(data: cell.post.author_pic)
        
        cell.caption_label.text = post.caption
        cell.post_image.image = UIImage(data: post.image)
        cell.likes_count.text = "\(post.likes.count) Likes"
        
        // Update heart icon
        let username = PFUser.current()!.username!
        let liked = cell.post.likes.contains(username)
        if liked{
            cell.like_button.setImage(UIImage(named: "like"), for: .normal)
        }
        else{
            cell.like_button.setImage(UIImage(named: "unlike"), for: .normal)
        }
        
        return cell
    }
    
    func safelyFetchPosts(){
        if !in_call{
            if !app_delegate.isConnected(){
                let network_alert = UIAlertController(
                    title: "Network Error",
                    message: "Please check your internet connection",
                    preferredStyle: .alert
                )
                
                network_alert.addAction(UIAlertAction(title: "Retry", style: .cancel) {UIAlertAction in
                    self.safelyFetchPosts()
                })
                
                self.present(network_alert, animated: true, completion: nil)
            }
            else{
                
                // Now it is safe to fetch posts
                
                in_call = true
                table_view.alpha = 0
                no_posts_yet.alpha = 0
                posts.removeAll()
                loader.startAnimating()
                
                let query = PFQuery(className: "Posts")
                query.whereKey("author", notEqualTo: PFUser.current()!.username!)
                query.findObjectsInBackground { (posts_list, error) in
                    if error != nil{
                        self.showSimpleAlert(title: "Error", message: (error?.localizedDescription)!)
                    }
                    else{
                        if posts_list!.count == 0{
                            self.no_posts_yet.alpha = 1
                        }
                        else{
                            self.table_view.alpha = 1
                        }
                        
                        for post in posts_list!{
                            let post_obj = Post()
                            post_obj.uuid = post["uuid"] as! String
                            post_obj.author = post["author"] as! String
                            post_obj.caption = post["caption"] as! String
                            post_obj.image = post["image"] as! Data
                            post_obj.likes = post["likes"] as! [String]
                            post_obj.author_pic = post["author_pic"] as! Data
                            
                            self.posts.append(post_obj)
                        }
                        
                        self.loader.stopAnimating()
                        self.table_view.reloadData()
                        self.in_call = false
                    }
                }
            }
        }
    }
    
    func safelyToggleLike(index: IndexPath){
        if !in_call{
            if !app_delegate.isConnected(){
                
                let network_alert = UIAlertController(
                    title: "Network Error",
                    message: "Please check your internet connection",
                    preferredStyle: .alert
                )
                
                network_alert.addAction(UIAlertAction(title: "Retry", style: .cancel) {UIAlertAction in
                    self.safelyToggleLike(index: index)
                })
                
                self.present(network_alert, animated: true, completion: nil)
            }
            else{
                
                // Now it is safe to toggle the like
                
                let cell = table_view.cellForRow(at: index) as! FeedCell
                let username = PFUser.current()!.username!
                let liked = cell.post.likes.contains(username)
                loader.startAnimating()
                
                let query = PFQuery(className: "Posts")
                query.whereKey("uuid", equalTo: cell.post.uuid)
                query.findObjectsInBackground { (results, error) in
                    if error != nil{
                        self.showSimpleAlert(title: "Error", message: (error?.localizedDescription)!)
                    }
                    else{
                        let target = results!.first!
                        var likes = target["likes"] as! [String]
                        
                        if !liked{
                            likes.append(username)
                        }
                        else{
                            if let index = likes.index(of: username) {
                                likes.remove(at: index)
                            }
                        }
                        
                        target["likes"] = likes
                        target.saveInBackground(block: { (success, error) in
                            if error != nil{
                                self.showSimpleAlert(title: "Error", message: (error?.localizedDescription)!)
                            }
                            else if success{
                                if let index = cell.post.likes.index(of: username) {
                                    cell.post.likes.remove(at: index)
                                }
                                
                                if liked{
                                    cell.like_button.setImage(UIImage(named: "unlike"), for: .normal)
                                }
                                else{
                                    cell.like_button.setImage(UIImage(named: "like"), for: .normal)
                                    cell.post.likes.append(username)
                                }
                                
                                cell.likes_count.text = "\(cell.post.likes.count) Likes"
                            }
                            
                            self.loader.stopAnimating()
                            self.in_call = false
                        })
                    }
                }
            }
        }
    }
    
    func safelyLogout(){
        if !in_call{
            if !app_delegate.isConnected(){
                let network_alert = UIAlertController(
                    title: "Network Error",
                    message: "Please check your internet connection",
                    preferredStyle: .alert
                )
                
                network_alert.addAction(UIAlertAction(title: "Retry", style: .cancel) {UIAlertAction in
                    self.safelyLogout()
                })
                
                self.present(network_alert, animated: true, completion: nil)
            }
            else{
                
                // Now it is safe to logout
                
                loader.startAnimating()
                no_posts_yet.alpha = 0
                
                PFUser.logOutInBackground { (error) in
                    if error != nil{
                        self.showSimpleAlert(title: "Error", message: (error?.localizedDescription)!)
                    }
                    else{
                        UserDefaults.standard.removeObject(forKey: "email")
                        UserDefaults.standard.synchronize()
                        
                        let app_delegate = UIApplication.shared.delegate as! AppDelegate
                        let board = UIStoryboard(name: "Main", bundle: nil)
                        let login_vc = board.instantiateViewController(withIdentifier: "login_vc") as! LoginViewController
                        app_delegate.window?.rootViewController = login_vc
                    }
                    
                    self.loader.stopAnimating()
                    self.in_call = false
                }
            }
        }
    }

}
