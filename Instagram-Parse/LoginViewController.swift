//
//  LoginViewController.swift
//  Instagram-Parse
//
//  Created by Ibraheem Hindi on 8/23/18.
//  Copyright © 2018 Ibraheem Hindi. All rights reserved.
//

import UIKit
import Parse

class LoginViewController: UIViewController {
    
    @IBOutlet weak var email_field: UITextField!
    @IBOutlet weak var password_field: UITextField!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    
    
    func showSimpleAlert(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel) {
            UIAlertAction in
            
            if title == "Success"{
                self.storeUser()
            }
        })
        self.present(alert, animated: true, completion: nil)
    }
    
    func isValidEmail(email_str: String) -> Bool {
        let regex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        return NSPredicate(format:"SELF MATCHES %@", regex).evaluate(with: email_str)
    }
    
    func storeUser(){
        // Store user's email locally
        UserDefaults.standard.set(self.email_field.text!, forKey: "email")
        UserDefaults.standard.synchronize()
        
        // Go to profile
        self.performSegue(withIdentifier: "toProfile", sender: nil)
    }
    
    @objc func hideKeyboard() {
        self.view.endEditing(true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Tap anywhere to hide the keyboard
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        self.view.addGestureRecognizer(recognizer)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        segue.destination.modalTransitionStyle = .crossDissolve
    }
    
    @IBAction func login(_ sender: Any) {
        if email_field.text == ""{
            showSimpleAlert(title: "Error", message: "Please provide your email")
        }
        else if password_field.text == ""{
            showSimpleAlert(title: "Error", message: "Please provide your password")
        }
        else{
            loader.startAnimating()
            PFUser.logInWithUsername(
                inBackground: email_field.text!,
                password: password_field.text!,
                block: { (user, error) in
                    self.loader.stopAnimating()
                    if error != nil{
                        self.showSimpleAlert(title: "Error", message: (error?.localizedDescription)!)
                    }
                    else{
                        self.storeUser()
                    }
            })
        }
    }
    
    @IBAction func signup(_ sender: Any) {
        let email = email_field.text!
        let password = password_field.text!
        
        if email_field.text == ""{
            showSimpleAlert(title: "Error", message: "Please provide your email")
        }
        else if password_field.text == ""{
            showSimpleAlert(title: "Error", message: "Please provide your password")
        }
        else if !isValidEmail(email_str: email){
            showSimpleAlert(title: "Error", message: "Please provide a valid email")
        }
        else{
            let user = PFUser()
            user.username = email
            user.password = password
            
            let default_pic = UIImage(named: "user")
            user["pic"] = UIImageJPEGRepresentation(default_pic!, 0.5)!
            
            loader.startAnimating()
            user.signUpInBackground(block: { (success, error) in
                self.loader.stopAnimating()
                if error != nil{
                    self.showSimpleAlert(title: "Error", message: (error?.localizedDescription)!)
                }
                else{
                    self.showSimpleAlert(title: "Success", message: "Account created successfully")
                }
            })
        }
    }
    
    @IBAction func onEmailEntered(_ sender: Any) {
        if password_field.text == ""{
            password_field.becomeFirstResponder()
        }
        else{
            resignFirstResponder()
        }
    }
    
    @IBAction func onPasswordEntered(_ sender: Any) {
        if email_field.text == ""{
            email_field.becomeFirstResponder()
        }
        else{
            resignFirstResponder()
        }
    }

}
