//
//  FeedCell.swift
//  Instagram-Firebase
//
//  Created by Ibraheem Hindi on 8/19/18.
//  Copyright © 2018 Ibraheem Hindi. All rights reserved.
//

import UIKit
import Parse

class FeedCell: UITableViewCell {
    
    @IBOutlet weak var post_image: UIImageView!
    @IBOutlet weak var like_button: UIButton!
    @IBOutlet weak var likes_count: UILabel!
    @IBOutlet weak var author_label: UILabel!
    @IBOutlet weak var author_image: UIImageView!
    @IBOutlet weak var caption_label: UILabel!

    var feed_container: FeedViewController? = FeedViewController()
    var profile_container: ProfileViewController? = ProfileViewController()
    var index = IndexPath()
    var post = Post()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBAction func postLike(_ sender: Any) {
        if feed_container != nil{
            feed_container!.safelyToggleLike(index: index)
        }
        else if profile_container != nil{
            profile_container!.safelyToggleLike(index: index)
        }
    }
    
    @IBAction func postComment(_ sender: Any) {
        if feed_container != nil{
            feed_container!.goToComments(index: index)
        }
        else if profile_container != nil{
            profile_container!.goToComments(index: index)
        }
    }
    
}
