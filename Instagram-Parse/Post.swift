//
//  Post.swift
//  Instagram-Parse
//
//  Created by Ibraheem Hindi on 8/23/18.
//  Copyright © 2018 Ibraheem Hindi. All rights reserved.
//

import Foundation

class Post{
    var uuid = ""
    var author = ""
    var caption = ""
    var image = Data()
    var author_pic = Data()
    var likes = [String]()
    var comments = [Comment]()
    
    func toString() -> String{
        return "Author : \(author), Caption : \(caption)"
    }
}
