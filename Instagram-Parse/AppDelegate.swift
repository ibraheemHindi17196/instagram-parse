//
//  AppDelegate.swift
//  Instagram-Parse
//
//  Created by Ibraheem Hindi on 8/23/18.
//  Copyright © 2018 Ibraheem Hindi. All rights reserved.
//

import UIKit
import Parse
import SystemConfiguration

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func setupParse(app_id: String, client_key: String, server_url: String){
        let config = ParseClientConfiguration { (ParseMutableClientConfiguration) in
            ParseMutableClientConfiguration.applicationId = app_id
            ParseMutableClientConfiguration.clientKey = client_key
            ParseMutableClientConfiguration.server = server_url
        }
        Parse.initialize(with: config)

        let ACL = PFACL()
        ACL.hasPublicReadAccess = true
        ACL.hasPublicWriteAccess = true
        PFACL.setDefault(ACL, withAccessForCurrentUser: true)
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        setupParse(
            app_id: "CustomParseServer",
            client_key: "a2S6UdtCKVv5v4bBQ88sqg==",
            server_url: "http://custom-parse-server.herokuapp.com/parse"
        )
        
        // UserDefaults.standard.removeObject(forKey: "email")
        // UserDefaults.standard.synchronize()
        
        // If a user is logged in, skip login
        if (UserDefaults.standard.value(forKey: "email") as? String) != nil {
            let board: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            window?.rootViewController = board.instantiateViewController(withIdentifier: "profile_vc") as! UITabBarController
        }
        
        return true
    }
    
    func isConnected() -> Bool{
        var zero_address = sockaddr_in(
            sin_len: 0,
            sin_family: 0,
            sin_port: 0,
            sin_addr: in_addr(s_addr: 0),
            sin_zero: (0, 0, 0, 0, 0, 0, 0, 0)
        )
        
        zero_address.sin_len = UInt8(MemoryLayout.size(ofValue: zero_address))
        zero_address.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zero_address) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            return false
        }
        
        // Working for Cellular and WIFI
        let reachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needs_connection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        
        return reachable && !needs_connection
    }


    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

