//
//  CommentCell.swift
//  Instagram-Parse
//
//  Created by Ibraheem Hindi on 8/24/18.
//  Copyright © 2018 Ibraheem Hindi. All rights reserved.
//

import UIKit

class CommentCell: UITableViewCell {
    
    @IBOutlet weak var comment_label: UILabel!
    @IBOutlet weak var author_label: UILabel!
    @IBOutlet weak var time_label: UILabel!
    @IBOutlet weak var author_pic: UIImageView!
    
    var comment = Comment()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
