# instagram-parse

An instagram clone that relies on Parse platform for user authentication and the database. As a user I can :-

* Sign up, sign in and sign out.
* Update my profile picture (reflects in all my posts and comments)
* Create a new post (contains title and image)
* View my posts
* View feed (others' posts)
* Like posts
* Comment on posts
* Delete from my posts

The app also remembers user's login such that users do not have to enter their credentials every time they open the app.

Demo Video : https://www.dropbox.com/s/arweglbx5soadzj/1-Instagram-Parse.mp4?dl=0
